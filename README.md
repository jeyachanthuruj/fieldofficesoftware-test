# FieldofficesoftwareTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.


## Required
1. npm
2. angular-cli

## Installation
1. Clone the repo.
2. run `npm i`
3. run `ng serve` - for see the app.
4. run `ng test` - for run the unit test. 