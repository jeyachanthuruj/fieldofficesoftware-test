import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wordfilter'
})
export class WordfilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return this.filter(value, args);
  }

  private filter(string, badwords) {
    return string.split(/\b/).map(function(word) {
      let newWord = word;
      if ( word.match(/\S+/g) !== null && badwords.indexOf(word.toLowerCase()) >= 0 ) {
        newWord = word.split('').map( w => '*').join('');
      }
      return newWord;
    }.bind(this)).join('');
  }

}
