import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { WordfilterPipe } from './wordfilter.pipe';
import { DataService, MockData } from './data.service';

describe('AppComponent', () => {

  let fixture,
    component,
    dataService,
    el;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        WordfilterPipe
      ],
      providers: [
        DataService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    dataService = TestBed.get(DataService);
    el = fixture.debugElement.nativeElement.querySelector('h1');
  }));

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it(`should have as title 'Word censor using Angular PIPE'`, async(() => {
    expect(component.title).toEqual('Word censor using Angular PIPE');
  }));
});
