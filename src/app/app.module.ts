import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WordfilterPipe } from './wordfilter.pipe';
import { DataService } from './data.service';


@NgModule({
  declarations: [
    AppComponent,
    WordfilterPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [DataService],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
