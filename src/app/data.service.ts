import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor() { }

  mockDataReq() {
    return new Promise(resolve => {
      const data = {
          'message': 'This is a bad word contain message',
          'words': ['the', 'are', 'bad', 'words']
        };
        resolve(data);
    });
  }

}


export class MockData {
  message: string;
  words: string[];
}
