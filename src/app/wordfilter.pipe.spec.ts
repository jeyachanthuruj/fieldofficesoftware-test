import { WordfilterPipe } from './wordfilter.pipe';

const badWords = ['cencered', 'are', 'bad', 'words'];

describe('WordfilterPipe', () => {

  let pipe: WordfilterPipe;

  beforeEach(() => {
    pipe = new WordfilterPipe();
  });

  it(`Should be censored 3 bad words`, () => {
    expect(pipe.transform('All bad words are here.', badWords)).toEqual('All *** ***** *** here.');
  });

  it(`Should be censored the bad word with comma 'bad,'`, () => {
    expect(pipe.transform('bad,', badWords)).toEqual('***,');
  });

  it(`Should be censored the bad word with fullstop 'bad.'`, () => {
    expect(pipe.transform('bad.', badWords)).toEqual('***.');
  });

  it(`Should not be censored the bad word with substring 'badland'`, () => {
    expect(pipe.transform('badland', badWords)).toEqual('badland');
  });

  it(`Should be censored the bad word with Uppercase letters 'BAD'`, () => {
    expect(pipe.transform('BAD', badWords)).toEqual('***');
  });

  it(`Should be censored the bad word with Lowercase letters 'bad'`, () => {
    expect(pipe.transform('BAD', badWords)).toEqual('***');
  });
});
