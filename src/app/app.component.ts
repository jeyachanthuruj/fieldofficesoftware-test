import { Component, OnInit } from '@angular/core';
import { DataService, MockData } from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'Word censor using Angular PIPE';
  public data: MockData;

  constructor(
    private service: DataService
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.mockDataReq().then(
      (res: MockData) => this.data = res
    );
  }

}
